PlayJam GameStick API server
****************************

Makes PlayJam GameSticks usable after the official server had been shut down
in 2017.


Setup
=====

1. Copy ``config.php.dist`` to ``config.php``
2. Clone the `game data repository <https://codeberg.org/gamestick-fans/game-data>`_::

     $ git clone https://codeberg.org/gamestick-fans/game-data.git
3. Unpack one of the cache files::

     $ cd game-data
     $ gunzip -k caches/2017-01-12T00\:19\:12-alex.shorts.pretty.json.gz
4. Generate the popular and featured menus from the cache file::

     $ ./bin/list-popular.php 'caches/2017-01-12T00:19:12-alex.shorts.pretty.json' > ../cache/popular.txt
     $ ./bin/list-featured.php 'caches/2017-01-12T00:19:12-alex.shorts.pretty.json' > ../cache/featured.json
5. Fill the caches for this API::

     $ cd ..
     $ ./bin/generate-apps-cache.php game-data/classic/
6. Setup the Apache web server and point the virtual host to the ``www/`` directory.


About
=====
This server software was written by `Christian Weiske <https://cweiske.de/>`_
and is licensed under the AGPL v3.
