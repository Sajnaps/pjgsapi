<?php
/**
 * Database row from the "gamesticks" table
 */
class Profile
{
    public int $id;
    public string $hwId;
    public string $sessionId;
    public ?string $verificationCode;
    public ?string $gamerTag;
    public bool $founderFlag;
    public ?string $founderName;
    public int $minAge;
    public ?string $avatar;

    public string $created_at;

    public function complete(): bool
    {
        return $this->gamerTag !== null;
    }

    public function getAvatarLargeUrl(): string
    {
        if (strpos($this->avatar, '://')) {
            return $this->avatar;
        }
        return 'http://l2.gamestickservices.net/resources/avatars/'
            . $this->avatar . '.large.jpg';
    }

    public function getAvatarSmallUrl(): string
    {
        if (strpos($this->avatar, '://')) {
            return $this->avatar;
        }
        return 'http://l2.gamestickservices.net/resources/avatars/'
            . $this->avatar . '.small.jpg';
    }
}
