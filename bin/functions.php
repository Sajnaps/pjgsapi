<?php
function loadConfigPopularTxtFile()
{
    if (!$GLOBALS['popuplarTxtFile']) {
        return;
    }

    if (!file_exists($GLOBALS['popuplarTxtFile'])) {
        throw new \Exception('popuplarTxtFile does not exist');
    }

    $GLOBALS['popular'] = file(
        $GLOBALS['popuplarTxtFile'], FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES
    );
}

function loadConfigFeaturedFile()
{
    if (!$GLOBALS['featuredFile']) {
        return;
    }

    if (!file_exists($GLOBALS['featuredFile'])) {
        throw new \Exception('featuredFile does not exist');
    }

    $featured = json_decode(file_get_contents($GLOBALS['featuredFile']));
    if ($featured === null) {
        throw new \Exception('cache/featured.json cannot be loaded');
    }

    foreach ($featured as $age => $packages) {
        $GLOBALS['featured'][$age] = $packages;
    }
}

/**
 * Load game data files and return them
 */
function loadGames(array $gameFiles): array
{
    $games = [];
    foreach ($gameFiles as $gameFile) {
        $gameData = json_decode(file_get_contents($gameFile));
        if ($gameData === null) {
            throw new \Exception('Cannot load game file: ' . $gameFile);
        }
        $games[$gameData->package] = $gameData;
    }
    return $games;
}

/**
 * Convert a meta data game info into a structure that is suitable
 * for http://l2.gamestickservices.net/api/rest/connect/stick/stick/xxx/view.json
 */
function convertGameDataForConnect(object $gameData, array $popular): array
{
    $userCurrency = 'EUR';

    $connectData = [
        'id'          => $gameData->id,
        'package'     => $gameData->package,
        'name'        => $gameData->name,
        'description' => $gameData->description,

        'minAge' => $gameData->minAge,
        'genre'  => current($gameData->genres),
        'genres' => [],

        'version'     => '',
        'gameversion' => '0',
        'size'        => 0,
        'download'    => [
            'url'     => '',
            'version' => 0,
        ],

        'popular'  => $popular[$gameData->package] ?? 0,
        'featured' => 0,

        'bought'         => true,
        'downloadedfree' => false,

        'isfree' => $gameData->prices->buy->$userCurrency->amount == 0,
        'pricing' => [
            'buy' => [
                'price' => $userCurrency . ' '
                    . $gameData->prices->buy->$userCurrency->amount,
            ],
            'rent' => [],
        ],
        'multipricing' => [
            'buy' => [],
            'rent' => [],
        ],

        'images' => [],
        'videos' => [],

        'social' => [],
        'companyname' => $gameData->companyname,
        'companylogofile' => '',
        'companyiconsize' => 0,
    ];

    //genres
    foreach ($gameData->genres as $genre) {
        $connectData['genres'][] = [
            'genre' => $genre,
        ];
    }

    //multipricing
    foreach ($gameData->prices->buy as $currencySymbol => $price) {
        $connectData['multipricing']['buy'][] = [
            'amount'            => $price->amount,
            'isocurrency'       => $currencySymbol,
            'symbol'            => $price->symbol,
            'symbolpostindex'   => $price->symbolpostindex,
            'formattedcurrency' => $price->formattedcurrency,
        ];
    }

    //download
    $highestVersionCode = 0;
    $highestVersionKey  = null;
    foreach ($gameData->releases as $releaseKey => $release) {
        if (isset($release->broken) && $release->broken === true) {
            continue;
        }
        if ($release->versionCode > $highestVersionCode) {
            $highestVersionCode = $release->versionCode;
            $highestVersionKey = $releaseKey;
        }
    }
    if ($highestVersionKey !== null) {
        $release = $gameData->releases[$highestVersionKey];
        $connectData['version']     = $release->uuid;
        $connectData['gameversion'] = $release->gsName ?? $release->name;
        $connectData['size']        = round($release->size / 1024 / 1024 * 1000);
        $connectData['download']    = [
            'url'     => $release->url,
            'version' => $release->versionCode,
        ];
    } else {
        $connectData['name'] = '!! ' . $connectData['name'];
    }

    foreach ($gameData->videos as $videoNum => $video) {
        $connectData['videos'][] = [
            'version' => $video->version,
            'url'     => $video->url,
        ];
        $connectData['images'][] = [
            'urls' => [
                [
                    'url'     => $video->thumb,
                    'version' => 1,
                ]
            ],
            'name'   => 'STICK_VIDEO' . ($videoNum + 1) . '_SCREENSHOT',
            'width'  => 350,
            'height' => 160,
        ];
    }

    $imageNameMap = [
        'logo-1'            => [
            'name'          => 'STICK_THUMBNAIL1',
            'width'         => 350,
            'height'        => 88,
        ],
        'logo-2'            => [
            'name'          => 'STICK_THUMBNAIL2',
            'width'         => 350,
            'height'        => 160,
        ],
        'logo-3'            => [
            'name'          => 'STICK_THUMBNAIL3',
            'width'         => 350,
            'height'        => 236,
        ],
        'logo-4'            => [
            'name'          => 'STICK_THUMBNAIL4',
            'width'         => 350,
            'height'        => 400,
        ],
        'logo-1-big'        => [
            'name'          => 'STICK_THUMBNAIL1_1080',
            'width'         => 525,
            'height'        => 240,
        ],
        'logo-2-big'        => [
            'name'          => 'STICK_THUMBNAIL2_1080',
            'width'         => 525,
            'height'        => 240,
        ],
        'logo-3-big'        => [
            'name'          => 'STICK_THUMBNAIL3_1080',
            'width'         => 525,
            'height'        => 240,
        ],
        'logo-4-big'        => [
            'name'          => 'STICK_THUMBNAIL4_1080',
            'width'         => 525,
            'height'        => 240,
        ],
        'icon-registration' => [
            'name'          => 'STICK_REGISTRATION_GAME_ICON',
            'width'         => 200,
            'height'        => 200,
        ],
        'icon'              => [
            'name'          => 'STICK_ICON',
            'width'         => 85,
            'height'        => 48,
        ],
        'screenshots'       => [
            'name'          => 'STICK_SCREENSHOT',
            'width'         => 350,
            'height'        => 160,
        ],
        'screenshots-big'   => [
            'name'          => 'STICK_SCREENSHOT_1080',
            'width'         => 525,
            'height'        => 240,
        ],
    ];
    foreach ($gameData->images as $imageKey => $imageData) {
        if (!isset($imageNameMap[$imageKey])) {
            throw new \Exception('Unknown image key: ' . $imageKey);
        }
        if ($imageData === null) {
            continue;
        }
        $connectImage = [
            'name'   => $imageNameMap[$imageKey]['name'],
            'width'  => $imageNameMap[$imageKey]['width'],
            'height' => $imageNameMap[$imageKey]['height'],
            'urls'   => [],
        ];
        $urls = (array) $imageData;
        foreach ($urls as $imageUrl) {
            $connectImage['urls'][] = [
                'url'     => $imageUrl,
                'version' => 1,
            ];
        }
        $connectData['images'][] = $connectImage;
    }

    return $connectData;
}


/**
 * @param array $featured Array keys are the ages, values
 *                        are array of "package name => height"
 *
 * @return array Suitable for
 * http://l2.gamestickservices.net/api/rest/connect/stick/stick/xxx/view.json
 */
function buildFeaturedMenu(array $featured, array $games)
{
    $connectMenu = [];
    foreach ($featured as $age => $packages) {
        $connectAge = [
            'age'     => $age,
            'entries' => [],
        ];

        $columnNum  = 0;
        $columnData = null;
        $heightSum  = 10;
        foreach ($packages as $package => $height) {
            if ($heightSum + $height > 6) {
                $columnNum++;
                $heightSum = 0;
                if ($columnData) {
                    $connectAge['entries'][] = $columnData;
                }
                $columnData = [
                    'column'        => $columnNum,
                    'columnentries' => [],
                ];
            }
            if (!isset($games[$package])) {
                throw new \Exception('Unknown featured game: ' . $package);
            }
            $columnData['columnentries'][] = [
                'gameID'    => $games[$package]->id,
                'thumbnail' => $height,
                'column'    => $columnNum,
            ];
            $heightSum += $height;
        }
        if (count($columnData['columnentries'])) {
            $connectAge['entries'][] = $columnData;
        }

        $connectMenu[] = $connectAge;
    }

    return $connectMenu;
}
