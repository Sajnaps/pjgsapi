#!/usr/bin/env php
<?php
/**
 * Generate cache file with information about apps
 *
 * Needed by:
 * - www/api/rest/connect/stick/stick/generate.php
 */
require_once __DIR__ . '/functions.php';
require_once __DIR__ . '/../config.php';

$cacheDir = dirname(__FILE__, 2) . '/cache/';
if (!is_dir($cacheDir)) {
    if (!mkdir($cacheDir)) {
        fwrite(STDERR, "Failed to create cache dir: $cacheDir\n");
        exit(10);
    }
}

if (!isset($argv[1])) {
    fwrite(STDERR, "Pass the path to a \"folders\" file with game data json files folder names\n");
    exit(1);
}

$foldersFile = $argv[1];
if (!is_file($foldersFile)) {
    fwrite(STDERR, 'Given path is not a file: ' . $foldersFile . "\n");
    exit(1);
}

$baseDir   = dirname($foldersFile);
$gameFiles = [];
foreach (file($foldersFile) as $line) {
    $line = trim($line);
    if (strlen($line)) {
        if (strpos($line, '..') !== false) {
            fwrite(STDERR, 'Path attack in ' . $folder . "\n");
        }
        $folder = $baseDir . '/' . $line;
        if (!is_dir($folder)) {
            fwrite(STDERR, 'Folder does not exist: ' . $folder . "\n");
        }
        $gameFiles = array_merge($gameFiles, glob($folder . '/*.json'));
    }
}

$appsCacheFile         = $cacheDir . 'connect-apps.min.json';
$featuredAgesCacheFile = $cacheDir . 'connect-featured-ages.min.json';


$games = loadGames($gameFiles);
loadConfigFeaturedFile();
loadConfigPopularTxtFile();
//make it "package name => number"
array_unshift($GLOBALS['popular'], 'dummy');
$GLOBALS['popular'] = array_flip($GLOBALS['popular']);
unset($GLOBALS['popular']['dummy']);


$connectGames = [];
foreach ($games as $gameData) {
    $connectGames[] = convertGameDataForConnect($gameData, $GLOBALS['popular']);
}
file_put_contents(
    $appsCacheFile,
    json_encode($connectGames, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)
);


$featuredMenu = buildFeaturedMenu($GLOBALS['featured'], $games);
file_put_contents(
    $featuredAgesCacheFile,
    json_encode($featuredMenu, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)
);
?>
