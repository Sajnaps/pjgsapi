<?php
$rootDir = dirname(__FILE__, 2);
$tplDir = $rootDir . '/templates';

require_once $rootDir . '/config.php';
require $rootDir . '/src/ProfileDb.php';
$profileDb = new ProfileDb();

$error   = null;
$code    = '';
$profile = null;
if (isset($_REQUEST['code']) && trim($_REQUEST['code']) !== '') {
    $code = $_REQUEST['code'];

    $profile = $profileDb->getProfileByVerificationCode($code);
    if ($profile === null) {
        $error = 'Invalid code';
    }
}

if ($profile === null) {
    require $tplDir . '/activate-code.phtml';
    exit();
}

$input = [
    'gamerTag'    => $_POST['gamerTag'] ?? null,
    'founderFlag' => (bool) ($_POST['founderFlag'] ?? false),
    'founderName' => $_POST['founderName'] ?? null,
    'minAge'      => $_POST['minAge'] ?? 3,
    'avatar'      => $_POST['avatar'] ?? 'avatar_1',
    'submit'      => $_POST['submit'] ?? false,
];

$avatars = [];
foreach (glob(__DIR__ . '/../www/resources/avatars/*.small.jpg') as $smallImage) {
    $key = basename($smallImage, '.small.jpg');
    $avatars[$key] = '/resources/avatars/' . basename($smallImage);
}

//input validation
$errors = [];
if (!preg_match('#^[A-Za-z0-9 ]+$#', $input['gamerTag'])) {
    $errors['gamerTag'] = 'Invalid gamer tag';
}
if ($input['founderFlag']) {
    if ($input['founderName'] === '') {
        $errors['founderName'] = 'Founder name missing';
    } else if (!preg_match('#^[A-Za-z0-9 ]+$#', $input['founderName'])) {
        $errors['founderName'] = 'Invalid founder name';
    }
}
if (!in_array($input['minAge'], [3, 7, 12, 17])) {
    $errors['minAge'] = 'Invalid age';
}
if (!in_array($input['avatar'], array_keys($avatars))) {
    $errors['avatar'] = 'Invalid avatar image';
}

if (!$input['submit'] || count($errors)) {
    require $tplDir . '/activate-profile.phtml';
    exit();
}

//validation successful, store the profile
//$input['verificationCode'] => null;
unset($input['submit']);
$profile = $profileDb->updateProfile($profile->hwId, $input);

require $tplDir . '/activate-success.phtml';
