<?php
/**
 * Generate user profile information at
 * GET http://l2.gamestickservices.net/api/rest/player/profile/view.json
 */
header('HTTP/1.0 500 Internal Server Error');

$rootDir = dirname(__FILE__, 5);
require_once $rootDir . '/config.php';
require_once $rootDir . '/src/ProfileDb.php';

if (!isset($_GET['jsessionid'])) {
    header('HTTP/1.0 400 Bad Request');
    header('Content-Type: text/plain');
    echo "Session ID missing\n";
    exit(1);
}

$profileDb = new ProfileDb();

$sessionId = $_GET['jsessionid'];
$profile = $profileDb->getProfileBySessionId($sessionId);
if ($profile === null) {
    header('HTTP/1.0 404 Not Found');
    header('Content-Type: text/plain');
    echo "Unknown session ID\n";
    exit(1);
}

if (count($GLOBALS['whitelistedHardwareIds'])
    && array_search($profile->hwId, $GLOBALS['whitelistedHardwareIds']) === false
) {
    header('HTTP/1.0 403 Forbidden');
    header('Content-Type: text/plain');
    echo "Gamestick with this hardware ID may not use this server\n";
    exit(1);
}

if (!$profile->complete()) {
    header('HTTP/1.0 404 Forbidden');
    header('Content-Type: text/plain');
    echo "Registration has not been completed yet\n";
    exit(1);
}

$nowMilli = time() * 1000;
$data = [
    'sid'             => $profile->sessionId,
    'time'            => (string) $nowMilli,
    'lastaccessed'    => $nowMilli,
    'created'         => $nowMilli,
    'accessCount'     => 0,
    'x-forwarded-for' => null,
    'addr'            => '1.2.3.4',
    'remoteaddr'      => '1.2.3.4',

    'body' => [
        'success' => true,
        'action'  => null,
        'message' => null,

        'accountType'    => 'CONSUMER',
        'gamertag'       => $profile->gamerTag,
        'avatarLargeUrl' => $profile->getAvatarLargeUrl(),
        'avatarSmallUrl' => $profile->getAvatarSmallUrl(),
        'minAge'         => $profile->minAge,
        'minAgeLabel'    => $profile->minAge . '+',

        'founderFlag'    => (int) $profile->founderFlag,
        'founderName'    => (string) $profile->founderName,

        'password'       => '81dc9bdb52d04dc20036dbd8313ed055',

        'location'       => 'GB',
        'currency'       => 'GBP',
        'dateOfBirth'    => '23/01/1970',
        'dateJoined'     => date('d/m/Y', strtotime($profile->created_at)),
        'email'          => 'dummy@example.org',

        'achievementStatus' => [
            'lastAchievementGameName'      => null,
            'numberOfAchievementsUnlocked' => 0,
        ],

        'balance' => [
            'amountOfMoneyLeft' => 'GBP 23.42',
            'transactions' => [
            ],
        ],

        'securityLevel' => 1,
    ],
];

$json = json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

header('HTTP/1.0 200 OK');
header('Content-Type: application/json');
echo $json . "\n";
