<?php
/**
 * Change the minimum age in the profile
 * GET http://l2.gamestickservices.net/api/rest/parentcontroll/change/agerating/17/7694f4a66316e53c8cdd9d9954bd611d/view.json;jsessionid=zz
 */
header('HTTP/1.0 500 Internal Server Error');

$rootDir = dirname(__FILE__, 6);
require_once $rootDir . '/config.php';
require_once $rootDir . '/src/ProfileDb.php';

if (!isset($_GET['jsessionid'])) {
    header('HTTP/1.0 400 Bad Request');
    header('Content-Type: text/plain');
    echo "Session ID missing\n";
    exit(1);
}
$sessionId = $_GET['jsessionid'];

if (!isset($_GET['age'])) {
    header('HTTP/1.0 400 Bad Request');
    header('Content-Type: text/plain');
    echo "age missing\n";
    exit(1);
}
$age = $_GET['age'];
if ($age != 3 && $age != 7 && $age != 12 && $age != 17) {
    header('HTTP/1.0 400 Bad Request');
    header('Content-Type: text/plain');
    echo "Invalid age (only 3, 7, 12 and 17 allowed)\n";
    exit(1);
}

if (!isset($_GET['pwhash'])) {
    header('HTTP/1.0 400 Bad Request');
    header('Content-Type: text/plain');
    echo "Password hash missing\n";
    exit(1);
}
$passwordHash = $_GET['pwhash'];
if (strlen($passwordHash) != 32) {
    header('HTTP/1.0 400 Bad Request');
    header('Content-Type: text/plain');
    echo "Password hash must be 32 characters long\n";
    exit(1);
}

$profileDb = new ProfileDb();
$profile = $profileDb->getProfileBySessionId($sessionId);
if ($profile === null) {
    header('HTTP/1.0 404 Not Found');
    header('Content-Type: text/plain');
    echo "Unknown session ID\n";
    exit(1);
}

//we do not verify the actual password
// the hash is calculated md5($password)
$profileDb->updateProfile(
    $profile->hwId,
    [
        'minAge' => $age,
    ]
);


$data = [
    'body' => [
        'success' => true,
        'message' => null,
        'action'  => 'ChangeAgeRating',
    ],
];
$json = json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

header('HTTP/1.0 200 OK');
header('Content-Type: application/json');
echo $json . "\n";
