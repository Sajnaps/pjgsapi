<?php
/**
 * Generate the apps list + other information available at
 * http://l2.gamestickservices.net/api/rest/connect/stick/stick/xxx/view.json
 */
header('HTTP/1.0 500 Internal Server Error');

$rootDir = dirname(__FILE__, 4);
require_once $rootDir . '/config.php';

$cacheDir = $rootDir . '/cache/';

$nowMilli = time() * 1000;

if (!isset($_GET['hwid'])) {
    header('HTTP/1.0 400 Bad Request');
    header('Content-Type: text/plain');
    echo "Hardware ID is missing\n";
    exit(1);
}
$hwId = strtolower($_GET['hwid']);

if (count($GLOBALS['whitelistedHardwareIds'])
    && array_search($hwId, $GLOBALS['whitelistedHardwareIds']) === false
) {
    header('HTTP/1.0 403 Forbidden');
    header('Content-Type: text/plain');
    echo "Gamestick with this hardware ID may not use this server\n";
    exit(1);
}

$sessionId = null;
if (isset($_GET['jsessionid'])) {
    $sessionId = $_GET['jsessionid'];
} else if (isset($_COOKIE['JSESSIONID'])) {
    $sessionId = $_COOKIE['JSESSIONID'];
}

require $rootDir . '/src/ProfileDb.php';
$profileDb = new ProfileDb();
$profile = $profileDb->getProfileByHardwareId($hwId);

if ($profile === null || !$profile->complete()) {
    //unregistered gamestick
    if ($profile === null) {
        $profile = $profileDb->createProfile($hwId);
    }

    $data = [
        'sid'  => $profile->sessionId,
        'time' => (string) $nowMilli,
        'body' => [
            'status'           => 'CONNECTION_IN_PROGRESS',
            'registrationCode' => $profile->verificationCode,
        ]
    ];
    $json = json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    header('HTTP/1.0 200 OK');
    header('Content-Type: application/json');
    echo $json . "\n";
    exit(0);
}


$data = [
    'sid'             => $profile->sessionId,
    'time'            => (string) $nowMilli,
    'lastaccessed'    => $nowMilli,
    'x-forwarded-for' => null,
    'created'         => $nowMilli,
    'accessCount'     => 0,
    'addr'            => '1.2.3.4',
    'remoteaddr'      => '1.2.3.4',

    'body' => [
        'status' => 'CONNECTED',
        'config' => [
            'apps'   => 'FIXME_APPS',
            'global' => [
                'defaults' => [
                    'images'   => [],
                    'currency' => null,
                    'social'   => [],
                ],
                'uitranslation' => [
                    'version' => 0,
                    'country' => [],
                ],
                'newfeatured' => [
                    'ages' => 'FIXME_AGES',
                ],
            ],
        ]
    ]
];

$json = json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);


//inject apps
$appsCacheFile = $cacheDir . 'connect-apps.min.json';
if (!file_exists($appsCacheFile)) {
    header('Content-Type: text/plain');
    echo "Cache file missing: connect-apps.min.json\n";
    exit(1);
}
$json = str_replace('"FIXME_APPS"', file_get_contents($appsCacheFile), $json);


//inject featured apps
$featuredAgesCacheFile = $cacheDir . 'connect-featured-ages.min.json';
if (!file_exists($featuredAgesCacheFile)) {
    header('Content-Type: text/plain');
    echo "Cache file missing: connect-featured-ages.min.json\n";
    exit(1);
}
$json = str_replace('"FIXME_AGES"', file_get_contents($featuredAgesCacheFile), $json);


header('HTTP/1.0 200 OK');
header('Content-Type: application/json');
echo $json . "\n";
?>
